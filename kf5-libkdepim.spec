%global framework libkdepim

Name:           kf5-%{framework}
Version:        23.08.5
Release:        2
Summary:        Library for common kdepim apps
License:        GPLv2+
URL:            https://invent.kde.org/pim/%{framework}
Source0:        http://download.kde.org/stable/release-service/%{version}/src/%{framework}-%{version}.tar.xz

%{?qt5_qtwebengine_arches:ExclusiveArch: %{qt5_qtwebengine_arches}}

BuildRequires:  boost-devel
BuildRequires:  extra-cmake-modules >= %{kf5_ver}
BuildRequires:  kf5-rpm-macros >= %{kf5_ver} 
BuildRequires:  cmake(KF5I18n)
BuildRequires:  cmake(KF5Completion)
BuildRequires:  cmake(KF5KCMUtils)
BuildRequires:  cmake(KF5Codecs)
BuildRequires:  cmake(KF5JobWidgets)
BuildRequires:  cmake(KF5KIO)
BuildRequires:  cmake(KF5Wallet)
BuildRequires:  cmake(KF5IconThemes)
BuildRequires:  cmake(KF5ItemViews)

BuildRequires:  cmake(Qt5Designer)
BuildRequires:  cmake(Qt5UiTools)
BuildRequires:  qt5-qtbase-devel

Obsoletes:      kdepim-libs < 7:16.04.0
Conflicts:      kdepim-libs < 7:16.04.0
# kdepimwidgets designer plugin moved here
Conflicts:      kdepim-common < 16.04.0
# kcm_ldap moved here
Conflicts:      kaddressbook < 16.04.0

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{version}


%build
%{cmake_kf5}
%cmake_build


%install
%cmake_install

%find_lang %{name} --all-name --with-html


%ldconfig_scriptlets

%files -f %{name}.lang
%license LICENSES/*
%{_kf5_datadir}/qlogging-categories5/*%{framework}.*
%{_kf5_libdir}/libKPim5Libkdepim.so.5
%{_kf5_libdir}/libKPim5Libkdepim.so.5.*

%ldconfig_scriptlets akonadi

%files devel
%{_includedir}/KPim5/Libkdepim/
%{_kf5_libdir}/cmake/KF5Libkdepim/
%{_kf5_libdir}/cmake/KPim5Libkdepim/
%{_kf5_libdir}/cmake/KPim5MailTransportDBusService/
%{_kf5_libdir}/libKPim5Libkdepim.so
%{_kf5_archdatadir}/mkspecs/modules/qt_Libkdepim.pri
%{_kf5_libdir}/cmake/MailTransportDBusService/
%{_kf5_datadir}/dbus-1/interfaces/org.kde.addressbook.service.xml
%{_kf5_datadir}/dbus-1/interfaces/org.kde.mailtransport.service.xml
%{_kf5_qtplugindir}/designer/kdepim5widgets.so


%changelog
* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 23.08.5-2
- adapt to the new CMake macros to fix build failure

* Mon Mar 18 2024 peijiankang <peijiankang@kylinos.cn> - 23.08.5-1
- update verison to 23.08.5

* Mon Jan 08 2024 misaka00251 <liuxin@iscas.ac.cn> - 23.08.4-1
- Upgrade to 23.08.4

* Fri Aug 04 2023 peijiankang <peijiankang@kylinos.cn> - 23.04.3-1
- 23.04.3

* Mon Mar 13 2023 tanyulong <tanyulong@kylinos.cn> - 22.12.0-1
- Update package to version 22.12.0

* Thu Jul 28 2022 misaka00251 <misaka00251@misakanet.cn> - 22.08.0-1
- Init package
